'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var RestaurantSchema = new Schema({
  name: String,
  address: String,
  map: String,
  business_hours: String,
  estimate: String,
  credit_payment: Boolean,
  smoking_zone: Boolean,
  website: String
});

module.exports = mongoose.model('Restaurant', RestaurantSchema);
